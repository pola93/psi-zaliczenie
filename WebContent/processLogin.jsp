<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<c:set var="dataSource" value="jdbc/myh2"></c:set>
	<sql:query dataSource="${dataSource }" var="result">
        SELECT id_user, login, password FROM USERS WHERE login = ? AND password = ?;
        <sql:param value="${ param.uname}"></sql:param>
        <sql:param value="${ param.upassword}"></sql:param>
	</sql:query>
    <c:choose>
		<c:when test="${result.rows[0] == null }">
			<c:redirect url="/login.jsp">
				<c:param name="errMessage" value="Użytkownik o podanym loginie lub haśle nie istnieje."></c:param>
			</c:redirect>
		</c:when>
		<c:otherwise>
			<c:redirect url="/userHome.jsp">
				<c:set var="loggedUserId" value="${result.rows[0].id_user }" scope="session"  />
				<c:set var="loggedUser" value="${param.uname}" scope="session"  />
			</c:redirect>
		</c:otherwise>
	</c:choose>
</body>
</html>