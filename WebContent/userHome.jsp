<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
    <%@ page import="java.time.LocalDateTime" %>
    <%@page import="java.time.format.DateTimeFormatter"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:set var="dataSource" value="jdbc/myh2"></c:set>
	<c:set var="currentDate" value="<%= LocalDateTime.now().format(DateTimeFormatter.ofPattern(\"yyyy-MM-dd HH:mm:ss\")) %>"></c:set>
	Welcome
	<c:out value="${sessionScope.loggedUser}"></c:out>
	<br />
	<sql:query dataSource="${dataSource }" var="result">
        SELECT active, last_login FROM USERS WHERE login = ?;
        <sql:param value="${sessionScope.loggedUser}"></sql:param>
	</sql:query>
	<c:set value="${result.rows[0].active }" var="isActive"></c:set>
	<fmt:parseDate value="${result.rows[0].last_login }" pattern="yyyy-MM-dd HH:mm:ss" var="tmplastLoginDate"/>
	<fmt:formatDate value="${tmplastLoginDate}" var="lastLoginDate" pattern="dd-MM-yyyy HH:mm:ss"/>
	<c:out value="Ostatnio logged in ${lastLoginDate}"></c:out>
	<c:out value="${activationMessage }"></c:out>
	<c:if test="${! isActive }">
		<br/>
		Twoje konto nie zostało jeszcze aktywowane!
		<br/>
		<form action="activateAccount.jsp" method="post">
			Podaj kod by aktywować konto:<br> <input type="text" name="code" /><br>
			<input type="submit" value="Aktywuj">
		</form>
	</c:if>
	<form action="/PSI-system/login.jsp" method="get">
		<input type="submit" value="Wyloguj się" />
	</form>
	<c:if test="${ isActive }">
		<form action="/PSI-system/changePassword.jsp" method="get">
			<input type="submit" value="Zmień hasło" />
		</form>
	</c:if>
		<sql:update dataSource="${dataSource }" var="newloggedTime">
			UPDATE USERS SET last_login = ? WHERE login = ?;
			<sql:param value="${ currentDate}"></sql:param>
			<sql:param value="${ sessionScope.loggedUser}"></sql:param>
		</sql:update>
	
</body>
</html>