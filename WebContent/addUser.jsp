<%@page import="java.time.format.DateTimeFormatter"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@ page
	language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page import="java.time.LocalDate" %>
	<%@ page import="java.time.LocalDateTime" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<c:set var="dataSource" value="jdbc/myh2"></c:set>
<c:set var="currentDate" value="<%= LocalDateTime.now().format(DateTimeFormatter.ofPattern(\"yyyy-MM-dd HH:mm:ss\")) %>"></c:set>
<c:set var="expirationDate" value="<%= LocalDate.now().plusMonths(1).format(DateTimeFormatter.ofPattern(\"yyyy-MM-dd\")) %>"></c:set>
	<sql:query var="existinglogin" dataSource="${dataSource}" >
    	SELECT login from users where login = ?;
    	<sql:param value="${param.userLogin}" />
  	</sql:query >
  	<c:if test="${existinglogin.rows[0] != null }">
  		<c:redirect url="/registration.jsp">
  			<c:param name="errMessage" value="Użytkownik o loginie ${param.userLogin} już istnieje."></c:param>
  		</c:redirect>
  	</c:if>
	<c:choose>
		<c:when test="${not empty param.userLogin && not empty param.userPassword}">
			<sql:transaction dataSource="${ dataSource }">
				<sql:update var="newUser">
					insert into users (id_user, login, password, last_login, expiration_date) VALUES (null, ?, ?, ?, ?);
					<sql:param value="${param.userLogin}" />
					<sql:param value="${param.userPassword}" />
					<sql:param value="${currentDate}"/>
					<sql:param value="${expirationDate}"/>
				</sql:update>
				<sql:query var="nextIdTable" >
			    	SELECT LAST_INSERT_ID() as lastId
			  	</sql:query >
			</sql:transaction>
			<c:set var="user_id" value="${nextIdTable.rows[0].lastId}"></c:set>
			<sql:transaction dataSource="${ dataSource }">
				<sql:update var="newPassword">
					insert into user_passwords (id, password, creation_date, id_user) values (null, ?, ?, ?);
					<sql:param value="${param.userPassword}" />
					<sql:param value="${currentDate}"/>
					<sql:param value="${user_id}"/>
				</sql:update>
			</sql:transaction>
			<c:out value="Stworzono użytkownika ${param.userLogin}"></c:out> <br>
			<form action="/PSI-system/login.jsp" method="get">
				<input type="submit" value="Przejdź do strony logowania" />
			</form>
		</c:when>
		<c:otherwise>
        	<font color="#cc0000">Please enter mandatory information.</font>
    	</c:otherwise>
	</c:choose>
</body>
</html>