<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:remove var="loggedUser" scope="session" />
	<c:out value="${param.errMessage }"></c:out>
	<form action="processLogin.jsp" method="post">
		Login:<br> <input type="text" name="uname" /><br>
		Password:<br> <input type="password" name="upassword" /><br>
		<input type="submit" value="Zaloguj się">
	</form>
	<form action="/PSI-system/registration.jsp" method="get">
		<input type="submit" value="Zarejestruj się" />
	</form>
</body>
</html>