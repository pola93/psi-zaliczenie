<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@taglib
	uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%><%@ page
	language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%@page import="pl.imsi.bean.Registration"%>

	<c:choose>
		<c:when test="${ param.upassword != param.confirmupassowrd }">
			<c:redirect url="/error.jsp">
				<c:param name="message" value="Password confirmation mistake.">
				</c:param>
			</c:redirect>
		</c:when>
		<c:otherwise>
			<c:redirect url="/addUser.jsp">
				<c:param name="userLogin" value="${ param.uname}"></c:param>
				<c:param name="userPassword" value="${ param.upassword}"></c:param>
			</c:redirect>
		</c:otherwise>
	</c:choose>
</body>
</html>