<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
	<c:set var="dataSource" value="jdbc/myh2"></c:set>
	<sql:update dataSource="${dataSource }" var="result">
		UPDATE USERS SET active = ? where login = ?;
		<sql:param value="${true}" />
		<sql:param value="${ sessionScope.loggedUser}"></sql:param>
	</sql:update>
	<c:redirect url="/userHome.jsp">
		<c:set var="activationMessage" value="Dziękujemy za aktywację." />
	</c:redirect>
</body>
</html>