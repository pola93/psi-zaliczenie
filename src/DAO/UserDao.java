package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pl.imsi.bean.ConnectionFactory;
import pl.imsi.bean.PasswordBean;
import utils.DBUtils;

public class UserDao {

	private Connection connection;
	private Statement statement;
    private PreparedStatement preparedStatement;
    
    public UserDao() {}
    
    public void changeUserPassword(String userLogin, String newPassword) {
    	String query = "UPDATE USERS SET password = ? WHERE login = ?";
        try {
            connection = ConnectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, newPassword);
            preparedStatement.setString(2, userLogin);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
        	DBUtils.close(preparedStatement);
        	DBUtils.close(connection);
        }
    }
    
    public String getUserPassword(String userLogin) {
    	String query = "SELECT password FROM USERS WHERE login = '" + userLogin + "'";
    	ResultSet rs = null;
    	String password = null;
    	try {
    		connection = ConnectionFactory.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			if(rs.next()) {
				password = rs.getString("password");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtils.close(rs);
        	DBUtils.close(statement);
        	DBUtils.close(connection);
        }
    	
		return password;
    }
    
    public List<String> getUsersLastPasswords(int userId) {
    	String query = "SELECT password FROM USER_PASSWORDS WHERE id_user = '" + userId + "'";
    	ResultSet rs = null;
    	List<String> lastPassword = new ArrayList<>();
    	try {
    		connection = ConnectionFactory.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			while (rs.next()) {
				lastPassword.add(rs.getString("password"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtils.close(rs);
        	DBUtils.close(statement);
        	DBUtils.close(connection);
        }
    	return lastPassword;
    }
    
    public int getOldestPasswordId(int userId) {
    	String query = "SELECT id, password, creation_date FROM USER_PASSWORDS WHERE id_user = '" + userId + "'";
    	ResultSet rs = null;
    	List<PasswordBean> passwords = new ArrayList<>();
    	int passwordId = 0;
    	try {
    		connection = ConnectionFactory.getConnection();
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
			while (rs.next()) {
				passwords.add(new PasswordBean(rs.getInt("id"), 
						rs.getString("password"), 
						rs.getTimestamp("creation_date")));
			}
			Collections.sort(passwords);
			passwordId = passwords.get(0).getId();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBUtils.close(rs);
        	DBUtils.close(statement);
        	DBUtils.close(connection);
        }
    	return passwordId;
    }
    
    public void deleteOldestPassword(int passId) {
    	String query = "DELETE USER_PASSWORDS WHERE id = ?";
    	try {
            connection = ConnectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, passId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
        	DBUtils.close(preparedStatement);
        	DBUtils.close(connection);
        }
    }
    
    public void addPasswordToListDB(int userId, String password) {
    	String query = "INSERT INTO USER_PASSWORDS (id, password, creation_date, id_user) values (null, ?, ?, ?)";
        try {
            connection = ConnectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, password);
            preparedStatement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            preparedStatement.setInt(3, userId);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
        	DBUtils.close(preparedStatement);
        	DBUtils.close(connection);
        }
    }
    
}
