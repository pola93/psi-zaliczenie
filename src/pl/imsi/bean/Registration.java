package pl.imsi.bean;

public class Registration {
	private String uname, upassword, confirmupassowrd;

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getUpassword() {
		return upassword;
	}

	public void setUpassword(String upassword) {
		this.upassword = upassword;
	}

	public String getConfirmupassowrd() {
		return confirmupassowrd;
	}

	public void setConfirmupassowrd(String confirmupassowrd) {
		this.confirmupassowrd = confirmupassowrd;
	}
	
}
