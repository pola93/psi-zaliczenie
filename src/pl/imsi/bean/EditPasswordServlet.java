package pl.imsi.bean;

import java.io.IOException;
import java.util.List;

import dao.UserDao;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet({ "/EditPassword", "/editPassword.do" })
public class EditPasswordServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final int maxPasswords = 3;
	private final UserDao userDao = new UserDao();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		super.doGet(req, resp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String successUrl = "/userHome.jsp";
		String changePasswordUrl = "/changePassword.jsp";
		response.setCharacterEncoding("UTF-8");
		request.setCharacterEncoding("UTF-8");

		String userLogin = request.getSession().getAttribute("loggedUser").toString();
		int userLoginId = Integer.parseInt(request.getSession().getAttribute("loggedUserId").toString());
		String uoldPassword = request.getParameter("uoldPassword");
		String unewPassword = request.getParameter("unewPassword");
		String uconfirmupassowrd = request.getParameter("uconfirmupassowrd");
		String uoldPassFromDB = userDao.getUserPassword(userLogin);
		ServletContext context = getServletContext();

		if (uoldPassFromDB == null || (!unewPassword.equals(uconfirmupassowrd)) || (!uoldPassFromDB.equals(uoldPassword))) {
			request.setAttribute("passwordChangedMessage", "B��dnie powt�rzone has�o.");
			RequestDispatcher dispatcher = context.getRequestDispatcher(changePasswordUrl);
			dispatcher.forward(request, response);
			return;
		}
		
		List<String> lastPasswords = userDao.getUsersLastPasswords(userLoginId);

		for (String pass : lastPasswords) {
			if (pass.equals(unewPassword)) {
				request.setAttribute("passwordChangedMessage", "Has�o musi by� r�ne od 3 poprzednich");
				RequestDispatcher dispatcher = context.getRequestDispatcher(changePasswordUrl);
				dispatcher.forward(request, response);
				return;
			}
		}
		if(lastPasswords.size() >= maxPasswords) {
			int oldestPasswordId = userDao.getOldestPasswordId(userLoginId);
			userDao.deleteOldestPassword(oldestPasswordId);
		}
		userDao.changeUserPassword(userLogin, unewPassword);
		userDao.addPasswordToListDB(userLoginId, unewPassword);

		request.setAttribute("passwordChangedMessage", "Has�o zosta�o zmienione.");
		RequestDispatcher dispatcher = context.getRequestDispatcher(successUrl);
		dispatcher.forward(request, response);
		return;
	}
}
