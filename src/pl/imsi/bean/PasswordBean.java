package pl.imsi.bean;

import java.sql.Timestamp;

public class PasswordBean implements Comparable<PasswordBean> {
	private int id;
	private String password;
	private Timestamp creationDate;
	
	public PasswordBean(int id, String password, Timestamp creationDate) {
		super();
		this.id = id;
		this.password = password;
		this.creationDate = creationDate;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Timestamp getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	@Override
	public int compareTo(PasswordBean o) {
		return getCreationDate().compareTo(o.getCreationDate());
	}
}
